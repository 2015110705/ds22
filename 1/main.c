/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define STR_MAX 10
#define MAX		11

typedef struct {
	char			item[STR_MAX];	// 입력 문자열
	unsigned int	key;			// 키 변환 함수의 반환 값
}Element;

unsigned int	stringToInt(char*);
Element*		search(int);
int				h(int);
void			insert(Element*);
void			printHashTable();

Element**		hashTable = NULL;

int main()
{
	FILE*	pInputFile = fopen("input.txt", "r");
	char	buffer[STR_MAX];
	int		i;

	// check file open error 
	if (pInputFile == NULL) {
		fprintf(stderr, "file open error");
		exit(EXIT_FAILURE);
	}

	// memory allocation and initialization
	hashTable = (Element**)malloc(sizeof(Element*) * MAX);

	for (i = 0; i < MAX; i++) {
		hashTable[i] = NULL;
	}

	// input data
	printf("input strings : ");
	for (i = 0; !feof(pInputFile); i++) {
		Element* newElement = (Element*)malloc(sizeof(Element));
		fscanf(pInputFile, "%s", newElement->item);
		newElement->key = stringToInt(newElement->item);

		// insert element to hashing table
		insert(newElement);

		printf("%s ", newElement->item);
	}
	printf("\n\n");
	fclose(pInputFile);

	// print hashing table
	printHashTable();

	// search string
	printf("string to search >> ");
	scanf("%s", buffer);
	search((int)stringToInt(buffer));
	
	// memory free
	free(hashTable);

	return 0;
}

// simple additive approach to create a natural number
// that is within the integer range
unsigned int stringToInt(char *key)
{
	int number = 0;

	while (*key) {
		number += *key++;
	}

	return number;
}

// search the linear probing hash table ht
// (each bucket has exactly one slot) for k,
// if a pair with key k is found, return a pointer to this pair;
// otherwise, return NULL
Element* search(int key)
{
	int homeBucket, currentBucket;
	int count = 1;

	homeBucket = h(key);

	for (currentBucket = homeBucket; hashTable[currentBucket] 
	&& hashTable[currentBucket]->key != key; count++) {
		// treat the table as circular
		currentBucket = (currentBucket + 1) % MAX;

		// back to start point
		if (currentBucket == homeBucket) {
			printf("it doesn't exist! \n");
			return NULL;
		}
	}

	if (hashTable[currentBucket] != NULL) {
		if (hashTable[currentBucket]->key == key) {
			printf("item : %s, key : %d, the number of comparisions : %d \n",
				hashTable[currentBucket]->item, hashTable[currentBucket]->key, count);
			return hashTable[currentBucket];
		}
	}

	printf("it doesn't exist! \n");
	return NULL;
}

// insert element to hashing table
void insert(Element* newElement)
{
	int i;

	for (i = h(newElement->key);  ; i = h(i + 1)) {
		if (hashTable[i] == NULL) {
			break;
		}
		
		if (hashTable[i]->key == newElement->key) {
			fprintf(stderr, "중복된 key가 존재합니다.");
			exit(EXIT_FAILURE);
		}
	}

	if (hashTable[i] == NULL) {
		hashTable[i] = newElement;
	}
	else {
		fprintf(stderr, "더 이상 테이블에 추가할 수 없습니다.");
		exit(EXIT_FAILURE);
	}
}

// return calculation result of k % b
int h(int key)
{
	return key % MAX;
}

// print hashing table
void printHashTable()
{
	int i;

	printf("%9s%10s%5s \n"," ", "item", "key");
	for (i = 0; i < MAX; i++) {
		printf("ht[%2d] : ", i);
		if (hashTable[i] != NULL) {
			printf("%10s%5d", hashTable[i]->item, hashTable[i]->key);
		}
		printf("\n");
	}
	printf("\n");
}