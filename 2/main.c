/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define BUCKET 8

typedef struct {
	unsigned int key; // 키 변환 함수의 반환 값
}Element;

Element** hashTable = NULL;
unsigned int random[BUCKET];

void insert(Element* newElement);
int h(int);
void printHash();
Element* search(int);

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");
	int seed;
	int input;
	int i, j;
	
	// memory allocation and initialization
	hashTable = (Element**)malloc(sizeof(Element*) * BUCKET);

	for (i = 0; i < BUCKET; i++) {
		hashTable[i] = NULL;
	}

	printf("key sequence from file : ");
	for (i = 0; !feof(pInputFile); i++) {
		fscanf(pInputFile, "%d ", &input);
		printf("%5d ", input);
	}
	printf("\n");
	fseek(pInputFile, 0L, SEEK_SET);

	// create random sequence 
	for (i = 1; i < BUCKET; i++) {
		random[i] = rand() % (BUCKET - 1) + 1;
		for (j = 1; j < i; j++) {
			if (random[i] == random[j]) {
				i--;
				break;
			}
		}
	}

	// input seed value
	printf("input seed >> ");
	scanf("%d", &seed);

	srand(seed);

	// input data
	for (i = 0; !feof(pInputFile); i++) {
		Element* newElement = (Element*)malloc(sizeof(Element));
		fscanf(pInputFile, "%d ", &(newElement->key));
		insert(newElement);
	}

	printf("\n");
	for (i = 1; i < BUCKET; i++) {
		printf("randNum[%d] : %d \n", i, random[i]);
	}
	printf("\n");

	fclose(pInputFile);

	printHash();

	while (1) {
		printf("\ninput 0 to quit \n");
		printf("key to search >> ");
		scanf("%d", &input);

		if (input == 0) {
			break;
		}

		search(input);
	}

	return 0;
}

void insert(Element* newElement)
{
	int key = newElement->key;

	if (hashTable[h(key)] == NULL) {
		hashTable[h(key)] = newElement;
	}
	else {
		int i;

		if (hashTable[h(key)]->key == key) {
			fprintf(stderr, "중복된 key가 존재합니다.");
			exit(EXIT_FAILURE);
		}

		for (i = 1; i < BUCKET; i++) {
			if (hashTable[(h(key) + random[i]) % BUCKET] == NULL) {
				hashTable[(h(key) + random[i]) % BUCKET] = newElement;
				return;
			}

			if (hashTable[(h(key) + random[i]) % BUCKET]->key == key) {
				fprintf(stderr, "중복된 key가 존재합니다.");
				exit(EXIT_FAILURE);
			}
		}
		
		fprintf(stderr, "더 이상 추가할 수 없습니다.");
		exit(EXIT_FAILURE);
	}
}

int h(int key) 
{
	return key % BUCKET;
}

void printHash()
{
	int i;

	printf("%8s%5s \n", " ", "key");
	for (i = 0; i < BUCKET; i++) {
		printf("ht[%2d] : ", i);
		if (hashTable[i] != NULL) {
			printf("%3d ", hashTable[i]->key);
		}
		printf("\n");
	}
}

Element* search(int key)
{
	int homeBucket, currentBucket;
	int count = 1;
	int i;

	homeBucket = h(key);

	if (hashTable[homeBucket] != NULL) {
		if (hashTable[homeBucket]->key == key) {
			printf("key : %d, the number of comparisions : %d \n", 
				hashTable[homeBucket]->key, count);
			return hashTable[homeBucket];
		}
	}

	for (currentBucket = homeBucket, i = 1; hashTable[currentBucket]
		&& hashTable[currentBucket]->key != key; count++, i++) {
		currentBucket = (h(key) + random[i]) % BUCKET;

		if (currentBucket == homeBucket) {
			printf("it doesn't exist! \n");
			return NULL;
		}
	}

	if (hashTable[currentBucket] != NULL) {
		if (hashTable[currentBucket]->key == key) {
			printf("key : %d, the number of comparisions : %d \n",
				hashTable[homeBucket]->key, count);
			return hashTable[currentBucket];
		}
	}

	printf("it doesn't exist! \n");
	return NULL;
}